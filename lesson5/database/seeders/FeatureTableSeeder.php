<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Models\Comment;
use App\Models\User;


class FeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->delete();
        DB::table('features')->insert([
            [
                'id' => 1,
                'title' => 'Where does it come from?',
                'status' => 'Planned',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 2,
                'title' => 'Where can I get some?',
                'status' => 'Completed',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 3,
                'title' => 'What is Lorem Ipsum?',
                'status' => 'Requested',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 4,
                'title' => 'Where can I get some?',
                'status' => 'Requested',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 5,
                'title' => 'Where can I get some?',
                'status' => 'Requested',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'id' => 6,
                'title' => 'Where can I get some food? ',
                'status' => 'Requested',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 7,
                'title' => 'Where can I get some chairs? ',
                'status' => 'Planned',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 8,
                'title' => 'Where can I get some juice? ',
                'status' => 'Planned',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 9,
                'title' => 'Where can I get some cake? ',
                'status' => 'Completed',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'id' => 10,
                'title' => 'Where can I get some drink? ',
                'status' => 'Completed',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            
        ]);
    }
}
