<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Models\Comment;
use App\Models\Feature;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('features')->delete();
        // DB::table('features')->insert([
        //     [
        //         'id' => 1,
        //         // 'user_id' => 001,
        //         // //'name' => 'John Doe',
        //         // //'photo' => '',
        //         // //'email' => 'Pdoej@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 2,
        //         // 'user_id' => 002,
        //         // //'name' => 'John Quaye',
        //         // //'photo' => '',
        //         // //'email' => 'jquaye@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 3,
        //     //     'user_id' => 003,
        //     //    // 'name' => 'Mark Grey',
        //     //     //'photo' => '',
        //     //     //'email' => 'grey@example.com',
        //     //     //'password' => 'password',
        //     //     'created_at' => Carbon::now(),
        //     //     'updated_at' => Carbon::now(),
        //     ],

        //     [
        
        //         'id' => 4,
        //         // 'user_id' => 004,
        //         // //'name' => 'Sam Grey',
        //         // //'photo' => '',
        //         // //'email' => 'samg@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 5,
        //         // 'user_id' => 005,
        //         // //'name' => 'Paul Blay',
        //         // //'photo' => '',
        //         // //'email' => 'blayp@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],


        //     [
        //         'id' => 6,
        //     //     'user_id' => 006,
        //     //     //'name' => 'John Sowah',
        //     //     //'photo' => '',
        //     //     //'email' => 'sowah@example.com',
        //     //    // 'password' => 'password',
        //     //     'created_at' => Carbon::now(),
        //     //     'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 7,
        //         // 'user_id' => 007,
        //         // //'name' => 'Victor Mark',
        //         // //'photo' => '',
        //         // //'email' => 'markv@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 8,
        //     //     'user_id' => 800,
        //     //     //'name' => 'Philip Smith',
        //     //     //'photo' => '',
        //     //    // 'email' => 'smith@example.com',
        //     //     //'password' => 'password',
        //     //     'created_at' => Carbon::now(),
        //     //     'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 9,
        //         // 'user_id' => 901,
        //         // //'name' => 'Price Wan',
        //         // //'photo' => '',
        //         // //'email' => 'wan@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],

        //     [
        //         'id' => 10,
        //         // 'user_id' => 100,
        //         // //'name' => 'Jack Ore',
        //         // //'photo' => '',
        //         // //'email' => 'ore@example.com',
        //         // //'password' => 'password',
        //         // 'created_at' => Carbon::now(),
        //         // 'updated_at' => Carbon::now(),
        //     ],

            
        // ]);
    
    }
}
