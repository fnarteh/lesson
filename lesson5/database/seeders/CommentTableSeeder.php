<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Comment;
use App\Models\User;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();
        DB::table('comments')->insert(
            [
                [
                    'id' =>1,
                    'feature_id' => 123,
                    'user_id' => 001,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>2,
                    'feature_id' => 122,
                    'user_id' => 002,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>3,
                    'feature_id' => 121,
                    'user_id' => 003,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>4,
                    'feature_id' => 124,
                    'user_id' => 004,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>5,
                    'feature_id' => 154,
                    'user_id' => 005,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>6,
                    'feature_id' => 160,
                    'user_id' => 006,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>7,
                    'feature_id' => 170,
                    'user_id' => 007,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>8,
                    'feature_id' => 184,
                    'user_id' => 800,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>9,
                    'feature_id' => 199,
                    'user_id' => 901,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],

                [
                    'id' =>10,
                    'feature_id' => 200,
                    'user_id' => 100,
                    'comment' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
                
            ]);
    }
}
