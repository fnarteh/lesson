<?php

namespace Database\Factories;

use App\Models\Model;
use App\Models\Comment;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

$factory->define(Comment::class, function (Faker $faker){
    $date = $faker->dateTimeBetween('-10 years', 'now');

    return [
        'comment' => $faker->sentences(rand(1,6), true),
        'created_at' => $date,
        'updated_at'=> $date,
    ];
});

