<?php

namespace Database\Factories;

use App\Models\Feature;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;


$factory->define(Feature::class, function (Faker $faker)
{
    $title = $faker->randomElement(['Add', 'Fix', 'Improve']). ' '.implode(' ', $faker->words(rand(2,5)));

    return [
        'title' => $title,
        'status' => $faker->randomElement([
            'Requested',
            'Requested',
            'Requested',
            'Requested',
            'Requested',
            'Requested',
            'Requested',
            'Requested',
            'Requested',
            'Planned',
            'Completed',
            'Completed',
        ]),
        ];
});

// class FeatureFactory extends Factory
// {
//     /**
//      * The name of the factory's corresponding model.
//      *
//      * @var string
//      */
//     protected $model = Feature::class;

//     /**
//      * Define the model's default state.
//      *
//      * @return array
//      */
//     public function definition()
//     {
//         return [
//             //
//         ];
//     }
// }
