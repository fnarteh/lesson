<?php

namespace Database\Factories;

use App\Models\Login;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;


$factory->define(Login::class, function (Faker $faker){
    return [
        'ip_address' => $faker->ipv4,
        'created_at' => $faker->dateTimeThisDecade('now'),
    ];
});

// class LoginFactory extends Factory
// {
//     /**
//      * The name of the factory's corresponding model.
//      *
//      * @var string
//      */
//     protected $model = Login::class;

//     /**
//      * Define the model's default state.
//      *
//      * @return array
//      */
//     public function definition()
//     {
//         return [
//             //
//         ];
//     }
// }
