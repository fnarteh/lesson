<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class LoginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('logins')->delete();
        DB::table('logins')->insert(
            [ 
              ['id' =>1,
              'user_id' =>1,
              'ip_address' =>'1010100011010100',
              'created_at' => Carbon::now(),
            ],

              ['id' =>2,
              'user_id' =>2,
              'ip_address' =>'168.212. 226.204',
              'created_at' => Carbon::now(),
            ],

              ['id' =>3,
              'user_id' =>3,
              'ip_address' =>'16,777,216',
              'created_at' => Carbon::now(),
            ],

              ['id' =>4,
              'user_id' =>4,
              'ip_address' =>' 268,435,456',
              'created_at' => Carbon::now(),
            ],

              ['id' =>5,
              'user_id' =>5,
              'ip_address' =>'536,870,912',
              'created_at' => Carbon::now(),
            ],

            ['id' =>6,
              'user_id' =>6,
              'ip_address' =>'536,870,912,5675',
              'created_at' => Carbon::now(),
            ],

            ['id' =>7,
              'user_id' =>7,
              'ip_address' =>'536,870,912,5555',
              'created_at' => Carbon::now(),
            ],

            ['id' =>8,
              'user_id' =>8,
              'ip_address' =>'536,870,912,777',
              'created_at' => Carbon::now(),
            ],

            ['id' =>9,
              'user_id' =>9,
              'ip_address' =>'536,870,912,666',
              'created_at' => Carbon::now(),
            ],
            
            ]);
    }
}
