<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            [ 'id' => 1,
              'name' =>'John Doe',
              'email' => 'doej@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 2,
              'name' =>'Blake Black',
              'email' => 'black@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 3,
              'name' =>'Belinda Blue',
              'email' => 'blueb@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ], 

            [ 'id' => 4,
              'name' =>'Steve Doe',
              'email' => 'steve@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 5,
              'name' =>'Kwesi Wood',
              'email' => 'wood@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 6,
              'name' =>'Jane May',
              'email' => 'jmay@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 7,
              'name' =>'Mike Sam',
              'email' => 'sam@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 8,
              'name' =>'Jane Mettle',
              'email' => 'mettle@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],

            [ 'id' => 9,
              'name' =>'John Quaye',
              'email' => 'quayej@example.com',
              'password' => 'password',
              // 'created_at' => Carbon::now(),
            ],


        ]);
    }
}
