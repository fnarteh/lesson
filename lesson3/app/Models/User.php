<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Login;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function logins(){

        return $this->hasMany(Login::class);
    }

    public function lastLogin(){

        return $this->hasOne(Login::class)->latest()->limit(1);
    }

    public function scopeWithLastLogin($query){

        $query->addSelect(['last_login_id' => Login::select('id')
               ->whereColumn('user_id', 'users.id')
               ->latest()
                ->take(1) 
        ])
        ->with('lastLogin');


    }

    
}
