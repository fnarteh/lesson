<?php

namespace App\Http\Controllers;
use App\Http\Controllers\UsersController;
use App\Models\User;
use App\Models\Login;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(){

        $users = User::query()
            //    ->withLastLogin()
            ->with('lastLogin')
              ->orderBy('name')
              ->paginate();

        return view('users', ['users' => $users ]);      
    }
}
