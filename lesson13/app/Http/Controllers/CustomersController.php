<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function index()
    {
       Auth::login(User::where('name','Tom Smith')->first());

        $customers= Customer::query()
          ->visibleTo(Auth::user())
          ->with('salesRep')
          ->orderBy('name')
          ->paginate();

        return view('customers', ['customers' => $customers]);  
    }
}
