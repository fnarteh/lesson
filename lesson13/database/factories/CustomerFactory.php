<?php

namespace Database\Factories;

use App\Models\Customer;
use Faker\Generator as Faker;


$factory->define(Customer::class, function (Faker $faker){
    return[
        'name' => $faker->company,
        'city' => $faker->city,
        'state' => $faker->stateAbbr,
    ];

});

// class CustomerFactory extends Factory
// {
//     /**
//      * The name of the factory's corresponding model.
//      *
//      * @var string
//      */
//     protected $model = Customer::class;

//     /**
//      * Define the model's default state.
//      *
//      * @return array
//      */
//     public function definition()
//     {
//         return [
//             //
//         ];
//     }
// }
