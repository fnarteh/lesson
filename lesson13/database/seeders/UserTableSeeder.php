<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            [ 'id' => 1,
              'name' => 'Floxy Narteh',
              'email'=> 't@example.com',
              'password'=> 'clean',
              'is_owner' => 0, 

            ],

            [ 'id' => 2,
              'name' => 'Tina Jones',
              'email'=> 'tina@example.com',
              'password'=> 'clean',
              'is_owner' => 0, 

            ],

            [ 'id' => 3,
              'name' => 'Jane Aquah',
              'email'=> 'j@example.com',
              'password'=> 'clean',
              'is_owner' => 0, 

            ],

            [ 'id' => 4,
              'name' => 'Tom Smith',
              'email'=> 'omt@example.com',
              'password'=> 'clean',
              'is_owner' => 1, 

            ],

            [ 'id' => 5,
              'name' => 'Cane Bell',
              'email'=> 'cane@example.com',
              'password'=> 'clean',
              'is_owner' => 0, 

            ],

            [ 'id' => 6,
              'name' => 'True Smith',
              'email'=> 'true@example.com',
              'password'=> 'clean',
              'is_owner' => 0, 

            ],

            [ 'id' => 7,
            'name' => 'Floxy Narteh',
            'email'=> 't9@example.com',
            'password'=> 'clean',
            'is_owner' => 0, 

            ],

            [ 'id' => 8,
            'name' => 'Tom Smith',
            'email'=> 'm@example.com',
            'password'=> 'clean',
            'is_owner' => 1, 

            ],

            [ 'id' => 9,
            'name' => 'True Smith',
            'email'=> 'tr@example.com',
            'password'=> 'clean',
            'is_owner' => 0, 

            ],

            [ 'id' => 10,
            'name' => 'Tom Smith',
            'email'=> 'o@example.com',
            'password'=> 'clean',
            'is_owner' => 1, 

            ],

            [ 'id' => 11,
            'name' => 'Floxy Narteh',
            'email'=> 'to@example.com',
            'password'=> 'clean',
            'is_owner' => 0, 

            ],






        ]);
    }
}
