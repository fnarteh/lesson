<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();
        DB::table('customers')->insert([
            ['id'=>1,
             'sales_rep_id' => 1,
              'name'=> 'The Cobalt Partners',
              'city'=>'Accra',
              'state'=>'Greater Accra',
            ],

            ['id'=> 2,
            'sales_rep_id' => 2,
            'name'=> 'Serene',
            'city'=>'Kumasi',
            'state'=>'Ashante Region',
            ],

            ['id'=>3,
            'sales_rep_id' => 3,
            'name'=> 'University of Legon',
            'city'=>'Accra',
            'state'=>'Greater Accra',
            ],

            ['id'=>4,
            'sales_rep_id' => 4,
            'name'=> 'Ghana Revenue Authority',
            'city'=>'Kpone',
            'state'=>'Greater Accra',
            ],

            ['id'=>5,
            'sales_rep_id' => 5,
            'name'=> 'Regent University',
            'city'=>'Kasoa',
            'state'=>'Greater Accra',
            ],

            ['id'=>6,
            'sales_rep_id' => 6,
            'name'=> 'Absa Bank',
            'city'=>'Madina',
            'state'=>'Greater Accra',
            ],

            ['id'=>7,
             'sales_rep_id' => 1,
              'name'=> 'Presby Church',
              'city'=>'Accra',
              'state'=>'Greater Accra',
            ],

            ['id'=>8,
            'sales_rep_id' => 4,
            'name'=> 'GES',
            'city'=>'Ga Central',
            'state'=>'Greater Accra',
            ],

            ['id'=>9,
            'sales_rep_id' => 6,
            'name'=> 'Uni Bank',
            'city'=>'Madina',
            'state'=>'Greater Accra',
            ],

            ['id'=>10,
            'sales_rep_id' => 4,
            'name'=> 'Ghana Teachers Ass.',
            'city'=>'Kpone',
            'state'=>'Greater Accra',
            ],

            ['id'=>11,
             'sales_rep_id' => 1,
              'name'=> 'Johnson & Co.',
              'city'=>'Accra',
              'state'=>'Greater Accra',
            ],

            
        ]);
    }
}
