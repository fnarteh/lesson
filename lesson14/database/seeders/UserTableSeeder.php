<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->delete();
       DB::table('users')->insert([

        ['id' => 1,
         'first_name' => 'Paul',
         'last_name' => 'Blay',
          'email' => 'p@example.com',
          'password'=> 'password',

        ],

        ['id' => 2,
         'first_name' => 'Jane',
         'last_name' => 'Blay',
          'email' => 'j@example.com',
          'password'=> 'password',

        ],

        ['id' => 3,
         'first_name' => 'Chris',
         'last_name' => 'Smith',
          'email' => 'sc@example.com',
          'password'=> 'password',

        ],

        ['id' => 4,
         'first_name' => 'Emmanuel',
         'last_name' => 'Ofosu',
          'email' => 'ofosu@example.com',
          'password'=> 'password',

        ],

        ['id' => 5,
         'first_name' => 'Mike',
         'last_name' => 'Blake',
          'email' => 'blake@example.com',
          'password'=> 'password',

        ],
       
       
      ]);
    }
}
