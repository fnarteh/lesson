<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            ['id' => 1,
            'company_id'=>1,
            'password' => 'password',
            'first_name' => 'Collins',
            'last_name' => 'Mark',
            'email' => 'collins@yahoo.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
            
            [ 'id' => 2,
            'company_id'=>2,
            'password' => 'password',
            'first_name' => 'Mike',
            'last_name' => 'Owusu',
            'email' => 'mike@yahoo.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
            
            [ 'id' => 3,
            'company_id'=>3,
            'password' => 'password',
            'first_name' => 'Jane',
            'last_name' => 'Attah',
            'email' => 'jane@serene.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            ],
            
            ['id' => 4,
            'company_id'=>4,
            'password' => 'password',
            'first_name' => 'Paul ',
            'last_name' => 'Arthur',
            'email' => 'paul@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 5,
            'company_id'=>5,
            'password' => 'password',
            'first_name' => 'Frank ',
            'last_name' => 'Armah',
            'email' => 'frank@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 6,
            'company_id'=>6,
            'password' => 'password',
            'first_name' => 'Patricia ',
            'last_name' => 'Barns',
            'email' => 'pat@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
           

            ['id' => 7,
            'company_id'=>7,
            'password' => 'password',
            'first_name' => 'Peter ',
            'last_name' => 'Ansah',
            'email' => 'peter@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 8,
            'company_id'=>1,
            'password' => 'password',
            'first_name' => 'Karen',
            'last_name' => 'Clare',
            'email' => 'clare@outlook.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 9,
            'company_id'=>2,
            'password' => 'password',
            'first_name' => 'Jame ',
            'last_name' => 'Anann',
            'email' => 'jann@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id' => 10,
            'company_id'=>5,
            'password' => 'password',
            'first_name' => 'Floxy ',
            'last_name' => 'Narteh',
            'email' => 'fnarteh@thecobaltpartners.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 11,
            'company_id'=>5,
            'password' => 'password',
            'first_name' => 'Lilly ',
            'last_name' => 'Mensah',
            'email' => 'mensahlilly@thecobaltpartners.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id' => 12,
            'company_id'=>3,
            'password' => 'password',
            'first_name' => 'Kwesi ',
            'last_name' => 'Bonsrah',
            'email' => 'bonsrah@serene.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 13,
            'company_id'=>4,
            'password' => 'password',
            'first_name' => 'Drake ',
            'last_name' => 'Mark',
            'email' => 'dmark@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],


            ['id' => 14,
            'company_id'=>7,
            'password' => 'password',
            'first_name' => 'Selina ',
            'last_name' => 'Ogoo',
            'email' => 'ogoos@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],


            ['id' => 15,
            'company_id'=>6,
            'password' => 'password',
            'first_name' => 'Patrick ',
            'last_name' => 'Jones',
            'email' => 'jonesp@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 16,
            'company_id'=>7,
            'password' => 'password',
            'first_name' => 'Jane ',
            'last_name' => "O'jones",
            'email' => 'ojanep@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
            
        ]);
    }
}
