<?php

use App\Models\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


class PostFactory extends Factory{
    protected $model = Post::class;

    // $factory->define(Post::class, function (Faker $faker) {
    //     $title = substr($faker->sentence(), 0, -1);
    
    //     return [
    //         'title' => $title,
    //         'slug' => Str::slug($title),
    //         'body' => $faker->paragraphs(500, true),
    //         'published_at' => $faker->dateTimeThisDecade(),
    //     ];
    // });

    public function definition()
    {
        return [
            'author_id' => User::factory(),
            'title' => $this->faker->sentence,
            'excerpt' => $this->faker->sentence,
            'body' => $this->faker->paragraphs(500, true),
            'published_at' => $this->faker->dateTimeThisDecade(),
        ];
    }


}

