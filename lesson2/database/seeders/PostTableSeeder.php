<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->delete(); 
        DB::table('posts')->insert([
            ['id' => 1,
            'author_id' =>1,
            'name' => 'Samuel Taylor',
            'title' => 'What is Lorem Ipsum?',
            'slug' => 'What is Lorem Ipsum?',
            'body' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            'published_at' => Carbon::yesterday(),],

            ['id' => 2,
            'author_id' =>2,
            'name' => 'Richard Sam',
            'title' => 'Why do we use it?',
            'slug' => 'Why do we use it?',
            'body' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
            'published_at' => Carbon::yesterday(),],

            ['id' => 3,
            'author_id' =>3,
            'name' => 'Philip Nunoo',
            'title' => 'Why i do what i do ?',
            'slug' => 'Why i do what i do ?',
            'body' => 'This is to help you learn how to fish for yourself and be happy in your success. Attibuting your success to your own accomplishment',
            'published_at' => Carbon::yesterday(),],

            ['id' => 4,
            'author_id' =>4,
            'name' => 'Floxy Narteh',
            'title' => 'Why i cry for help ?',
            'slug' => 'Why i cry for help  ?',
            'body' => 'I cry for help because i always feel that is the best thing to do . But what i have learned is that there is joy when you work to fix your errors and you gain knowlegde after you have suffered . Although i feel a secod eye is sometimes good.',
            'published_at' => Carbon::yesterday(),],

            ['id' => 5,
            'author_id' =>5,
            'name' => 'Stephen Musoke',
            'title' => 'We are there to help?',
            'slug' => 'We are there to help ?',
            'body' => 'We know you have no experience but we are ther eto help. Do not hesitate to call on us when you encounter any error.',
            'published_at' => Carbon::yesterday(),],

        ]);
    }
}
