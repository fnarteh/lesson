<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['id'=> 1,
            'author_id' => 1,
            'name'=> 'Sam Taylor',
            'email'=> 'taylor@yahoo.com',
            'password'=>'password',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id'=> 2,
            'author_id' => 2,
            'name'=> 'Richard Sam',
            'email'=> 'samrich@yahoo.com',
            'password'=>'password',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id'=> 3,
            'author_id' => 3,
            'name'=> 'Philip Nunoo',
            'email'=> 'pnunoo@yahoo.com',
            'password'=>'fishyourself',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id'=> 4,
            'author_id' => 4,
            'name'=> 'Floxy Narteh',
            'email'=> 'narteh@yahoo.com',
            'password'=>'selfish',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id'=> 5,
            'author_id' => 5,
            'name'=> 'Stephen Musoke',
            'email'=> 'musoke@yahoo.com',
            'password'=>'password',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],






        ]);
    }
}
